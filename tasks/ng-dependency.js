/*
 * ng_dependency
 *
 *
 * Copyright (c) 2014 Mariia Trubii
 * Licensed under the MIT license.
 */

'use strict';

var _ = require('underscore');

_.string = require('underscore.string');

module.exports = function (grunt) {

  // Please see the Grunt documentation for more information regarding task
  // creation: http://gruntjs.com/creating-tasks

  grunt.registerMultiTask(
    'ng_dependency',
    'Grab your controller dependencies into the JSON file',

    function () {

      var defaultOptions = {
          src: [ __dirname ],
          input: [],
          output: (__dirname + '/ng-dependency.json'),
          pattern: []
        },
        options = this.options(grunt.util._.merge({}, defaultOptions, this.data)),

        sourceFiles,
        sourceMap,
        inputFiles,
        inputMap,
        dependencyMap = {},
        filenameRegex = /\/([^/]*)$/,

        /**
         * Get dependencies for path.
         * @param {String} path
         * @returns {Array} Array or dependency file pathes
         */
        getPathDependencies = function (path) {
          var content = grunt.file.read(path).replace(/\r?\n|\r/gm, ""),
            matches = [],
            match,
            sourceKey,
            sourcePath,
            i,

            result;

          result = [];

          _.forEach(options.pattern, function (regex) {
            match = content.match(regex);

            if (match) {
              matches = matches.concat(match[1].replace(/\s/g, '').split(','));
            }

          });

          // Add dependency path to list, if it is present in Source map
          for (i = 0; i < matches.length; i++) {
            sourceKey = matches[i];
            sourcePath = sourceMap[sourceKey];

            if (sourcePath) {
              result.push(sourceMap[sourceKey]);
            }
          }

          return result;
        },

        /**
         * Get dependency files for nodes.
         * @param {Array} nodes
         * @param {Array} files
         */
        getDependencies = function (nodes) {
          var files = arguments[1] || nodes,
            pathes,
            resultPathesArray,
            i;

          _.forEach(nodes, function (path) {

            pathes = getPathDependencies(path);
            var deps = _.difference(pathes, files);
            _.forEach(pathes, function(dep) {
              if (-1 === files.indexOf(dep)) {
                files.push(dep);
              }
            });

            getDependencies(deps, files);
          });

          return files;
        },

        /**
         * Generates map from files path array
         * @param {Array} files
         * @returns {Object} {"filename": "filepath"}
         */
        getFilesMap = function (files) {
          var filename,
            map = {};

          _.forEach(files, function (path) {
            filename = path.match(filenameRegex)[1];
            filename = filename.substr(0, filename.lastIndexOf('.')) || filename;

            map[filename] = path;
          });

          return map;
        };

      options.input = (options.input && options.input.length) ? options.input : options.src;

      if (!options.overrideDefaultPatterns || options.overrideDefaultPatterns === false) {
        options.pattern = _.union(
          options.pattern,
          [
            /function\s*\(([^\)]+)\)/
          ]
        );
      }

      grunt.log.write('Getting sourse files...');

      sourceFiles = grunt.file.expand.apply(this, options.src);

      if (sourceFiles.length > 0) {
        grunt.log.ok(sourceFiles.length, ' files found.');
      } else {
        grunt.fail.warn('No source files.');
      }

      inputFiles = grunt.file.expand.apply(this, options.input);

      if (inputFiles.length > 0) {
        grunt.log.ok(inputFiles.length, ' files found.');
      } else {
        grunt.fail.warn('No input files.');
      }

      grunt.log.write('Getting files map...');

      // Get sourse files map
      sourceMap = getFilesMap(sourceFiles);

      // Get input files map
      inputMap = getFilesMap(inputFiles);

      grunt.log.ok();

      grunt.log.write('Generating dependency map...');

      // Get dependency map
      _.forEach(inputMap, function (path, key) {
        dependencyMap[key] = getDependencies([path]);
      });

      grunt.log.ok();

      grunt.file.write(this.data.output, JSON.stringify(dependencyMap));

      grunt.log.ok('Output file saved.');
    }
  );
};
